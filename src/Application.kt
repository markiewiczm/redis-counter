package com.outlook.markiewiczm

import com.fasterxml.jackson.databind.SerializationFeature
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.*
import io.ktor.jackson.jackson
import io.ktor.locations.Locations
import io.ktor.request.path
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.routing
import io.lettuce.core.RedisClient
import io.lettuce.core.api.sync.RedisCommands
import org.slf4j.event.Level


fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    install(Locations) {
    }

    install(CallLogging) {
        level = Level.INFO
        filter { call -> call.request.path().startsWith("/") }
    }

    install(DataConversion)

    install(DefaultHeaders) {
        header("X-Engine", "Kotlin, the best programming language in this galaxy")
    }

    install(ForwardedHeaderSupport) // WARNING: for security, do not include this if not behind a reverse proxy
    install(XForwardedHeaderSupport) // WARNING: for security, do not include this if not behind a reverse proxy

    install(ContentNegotiation) {
        jackson {
            enable(SerializationFeature.INDENT_OUTPUT)
        }
    }

    val redis = getRedis()

    routing {
        get("/") {
            val current = redis.incr("counter")
            call.respond(mapOf("value" to current))
        }
    }
}

private fun Application.getRedis(): RedisCommands<String, String> {
    val redisPort = environment.config.property("ktor.redis.port").getString().toInt()
    val redisHost = environment.config.property("ktor.redis.host").getString()
    val redisClient = RedisClient.create("redis://$redisHost:$redisPort/0")
    val connection = redisClient.connect()
    return connection.sync()
}

class AuthenticationException : RuntimeException()
class AuthorizationException : RuntimeException()

