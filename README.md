# Redis Counter

### Konfiguracja

Aplikacja odczytuje następujące zmienne środowiskowe przy starcie, poniżej podane są też ich domyślne wartości:

```bash
REDIS_HOST="localhost"
REDIS_PORT="6379"
HTTP_PORT="8080"
```
