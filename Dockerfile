# syntax=docker/dockerfile:1
FROM eclipse-temurin:11 AS builder

ADD . /build
WORKDIR /build
RUN --mount=type=cache,target=/root/.gradle ./gradlew build
FROM eclipse-temurin:11

ENV APPLICATION_USER ktor
RUN adduser --disabled-password --gecos '' --uid 1034 $APPLICATION_USER && \
    mkdir /app && \
    chown -R $APPLICATION_USER /app
USER $APPLICATION_USER

COPY --from=builder /build/build/libs/redis-counter.jar /app/app.jar
COPY --from=bitnami/kubectl:1.27 /opt/bitnami/kubectl/bin/kubectl /usr/local/bin/kubectl
WORKDIR /app

CMD ["java", "-server", "-XX:+UnlockExperimentalVMOptions", "-XX:InitialRAMFraction=2", "-XX:MinRAMFraction=2", "-XX:MaxRAMFraction=2", "-XX:+UseG1GC", "-XX:MaxGCPauseMillis=100", "-XX:+UseStringDeduplication", "-jar", "app.jar"]
